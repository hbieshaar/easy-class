/**
 * This function makes the connections between the subclass,
 * and its baseclass.
 * @param {Function} ctor - the subclass constructor
 * @param {Function} superCtor - the baseclass constructor
 */
export function subClass(ctor, superCtor) {
    
    // Set the constructor to refer to the subclass constructor
    if ( ! Object.hasOwnProperty('constructor') )
        Object.defineProperty(ctor.prototype, 'constructor', {
            configurable: true,
            enumerable: false,
            writable: true,
            value: ctor,
        });

    // Chain prototypes: object properties/methods
    Object.setPrototypeOf(ctor.prototype, superCtor.prototype);

    // Chain constructors: static properties/methods
    Object.setPrototypeOf(ctor, superCtor);

}

/**
 * Copies all own properties (including non-enumerable, and symbol-keys)
 * from the source to the target
 * @param {Object} target - the target object
 * @param {Object} source - the source object with the 'own'
 * properties/methods to be added to the target object.  
 * NOTE: this includes symbol properties.
 * @param {object} target - the object on which the new definitions take place
 * @param {object} source - the object the definitions are taken from
 */
export function assignOwn(target, source) {

    // copy all own non-symbol-key properties (incl non-enumerable)
    Object.getOwnPropertyNames(source).forEach(n => {
        Object.defineProperty(target, n, Object.getOwnPropertyDescriptor(source, n));
    });

    // copy all own symbol-key properties
    Object.getOwnPropertySymbols(source).forEach(s => {
        Object.defineProperty(target, s, Object.getOwnPropertyDescriptor(source, s));
    });

}
