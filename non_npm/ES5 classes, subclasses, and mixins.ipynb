{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Classes, subclasses, and mixins\n",
    "\n",
    "This notebook describes an easy way to create classes,\n",
    "subclasses, and mixins using ES5 function classes.\n",
    "ES6 JavaScript introduced the keywords `class`, and\n",
    "`super`, which cause more problems than they solve,\n",
    "so I will not use them.\n",
    "\n",
    "It will introduce the functions in the module EasyClass, which make using ES5 classes, subclasses, and mixins a breeze."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## The Simplest ES5 Class\n",
    "\n",
    "We start defining a simple ES5 function class."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [
    {
     "ename": "",
     "evalue": "",
     "output_type": "error",
     "traceback": [
      "\u001b[1;31mFailed to start the Kernel. \n",
      "Kernel Javascript (Node.js) is not usable. Check the Jupyter output tab for more information. \n",
      "View Jupyter <a href='command:jupyter.viewOutput'>log</a> for further details."
     ]
    }
   ],
   "source": [
    "function Person() {}\n",
    "\n",
    "console.log('Prototype property names:',\n",
    "           Object.getOwnPropertyNames(Person.prototype));\n",
    "console.log('PropertyDescriptor for constructor',\n",
    "           Object.getOwnPropertyDescriptor(\n",
    "                Person.prototype, 'constructor'));"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "When we look at the properties available in the\n",
    "`Person.prototype`, we find the property `constructor`.\n",
    "From its `PropertyDescriptor`, we learn that the\n",
    "`constructor` property is **non-enumerable**.\n",
    "This fact is very important for the creation of the\n",
    "prototypes of classes, subclasses, and mixins."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## A Typical class\n",
    "\n",
    "Our `Person` class did not bring much to the table. Let's change that by adding a function to let the person say hello. This is typically done by adding a function to the `Person.prototype`."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "Person.prototype.hello = function () {\n",
    "    console.log('Hello, my name is', this.name);\n",
    "}\n",
    "\n",
    "jd = new Person();  // creates John Doe\n",
    "jd.hello();"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Although the output of the `jd.hello()` statement is correct, it is not what we want. When we do not explicitly define a name for a person, we want its name to default to 'John Doe'. The prototype of a class is a great place to store constants, which may be redefined by a specific object. A default value is such a constant."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "Person.prototype.name = 'John Doe';\n",
    "jd.hello();"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "And we can still assign a name on our person."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "jd.name = 'Jane Doe';\n",
    "jd.hello();"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Our current `Person` class reads:\n",
    "``` js\n",
    "function Person() {}\n",
    "Person.prototype.hello = function () {\n",
    "    console.log('Hello, my name is', this.name);\n",
    "}\n",
    "Person.prototype.name = 'John Doe';\n",
    "```\n",
    "\n",
    "In real classes, there will be very many functions, and properties assigned to the prototype. The new ES6 classes seem easier to read, because they remove a lot of the clutter. It is however easy to create an uncluttered ES5 alternative.\n",
    "\n",
    "### 1st prototype alternative\n",
    "\n",
    "``` js\n",
    "function Person() {}\n",
    "Person.prototype = {\n",
    "    constructor: Person,\n",
    "    hello() {\n",
    "        console.log('Hello, my name is', this.name);\n",
    "    },\n",
    "    name: 'John Doe',\n",
    "};\n",
    "```\n",
    "\n",
    "In this alternative, we completely replace the `Person.prototype`. As a consequence, we loose the original non-enumerable property `constructor`. We therefor add it to the properties in the new object. The price we pay is this property is now enumerable.\n",
    "\n",
    "### 2nd prototype alternative\n",
    "\n",
    "``` js\n",
    "function Person() {}\n",
    "Object.assign(Person.prototype, {\n",
    "    hello() {\n",
    "        console.log('Hello, my name is', this.name);\n",
    "    },\n",
    "    name = 'John Doe',\n",
    "});\n",
    "```\n",
    "\n",
    "Using this alternative we avoid the enumerable `constructor` property, by not replacing the original prototype. The price we pay is that the `Object.assign()` only copies only enumerable own properties. The 'own' properties are not an issue, but both the fact that it copies, and the fact that only the enumerable properties are copied, might be. In case the non-enumerable properties are an issue, the function `EasyClass.assignOwn()`, which is described in the next section, might solve the problem.\n",
    "\n",
    "### 3rd prototype alternative\n",
    "\n",
    "``` js\n",
    "function Person() {}\n",
    "Person.prototype = {\n",
    "    hello() {\n",
    "        console.log('Hello, my name is', this.name);\n",
    "    },\n",
    "    name: 'John Doe',\n",
    "};\n",
    "EasyClass.subClass(Person, Object);\n",
    "```\n",
    "\n",
    "This alternative uses the `EasyClass.subClass()` function to make the `Person` class a subclass of the `Object` class. This is totally unnecessary, but this function also creates a non-enumerable property `constructor`, if it does not exist on the prototype.\n",
    "\n",
    "The function `EasyClass.subClass()` will be fully explained in the section 'Subclasses'."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Static functions and variables\n",
    "\n",
    "Static functions can be added as a property of the class. For example, let's create a description for the `Person` class."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "Person.description = function () {\n",
    "    console.log('A \"Person\" is an object with a name,',\n",
    "                'which can say hello.');\n",
    "};\n",
    "\n",
    "Person.description();"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Static variables can also be added as properties of the class"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Subclasses\n",
    "\n",
    "In the examples used in this section, we will extend the `Person` class to a `SuperHero` class, and a `Villain` class.\n",
    "\n",
    "Subclasses are classes. Therefor we need to define the function that constructs them, their prototype properties (functions and variables), and their static functions and properties.\n",
    "\n",
    "Let's start with our `SuperHero` class:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "function SuperHero(heroName, abilities) {\n",
    "    this.heroName = heroName;\n",
    "    this.abilities = abilities;\n",
    "}\n",
    "SuperHero.prototype = {\n",
    "    showAblities() {\n",
    "        console.log(\"My hero name is\", this.heroName);\n",
    "        this.abilities.forEach(ability =>\n",
    "               console.log(' - I can', ability));\n",
    "    },\n",
    "}\n",
    "\n",
    "superman = new SuperHero('Superman', ['fly', 'see with X-rays']);\n",
    "superman.showAblities();\n",
    "console.log('The constructor:', superman.constructor);\n",
    "console.log('superman is a SuperHero:',\n",
    "            superman instanceof SuperHero);\n",
    "console.log('superman is a Person:',\n",
    "            superman instanceof Person)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We now have a `SuperHero` class, where the created objects lack a proper `constructor` property, and the are not yet `Person`'s.\n",
    "\n",
    "In order to make a subclass/baseclass connections we need to do 2 things:\n",
    " 1. call the baseclass constructor at the start of our subclass\n",
    "    constructor, and\n",
    " 2. make the proper prototype connections (3 steps).\n",
    "\n",
    "The steps are:\n",
    " 1. **Calling the baseclass constructor**\n",
    "\n",
    "    The normal way to call a `BaseClass()` function to initialize\n",
    "    the new object is as a first statement in the `SubClass()`\n",
    "    function:\n",
    "    ``` js\n",
    "    function SubClass(...arguments) {\n",
    "        BaseClass.call(this, ...arguments);\n",
    "        ...\n",
    "    ```\n",
    "\n",
    "    So we need to modify our `SuperHero()` function like\n",
    "    ``` js\n",
    "    function SuperHero(heroName, abilities) {\n",
    "        Person.call(this);\n",
    "        this.heroName = heroName;\n",
    "        this.abilities = abilities;\n",
    "    }\n",
    "    ```\n",
    "\n",
    "    The following 3 steps will be made in the function `EasyClass.subClass()`. Here we will fix the `constructor` property, and make the connections for the prototypes..\n",
    "\n",
    "\n",
    " 2. **Fixing the `constructor`**\n",
    "\n",
    "    ``` js\n",
    "    if ( ! Object.hasOwnProperty('constructor') )\n",
    "        Object.defineProperty(ctor.prototype, 'constructor', {\n",
    "            configurable: true,\n",
    "            enumerable: false,\n",
    "            writable: true,\n",
    "            value: ctor,\n",
    "        });\n",
    "    ```\n",
    "    Although this is not strictly speaking a part of making the connections between a baseclass and a subclass, it is convenient to create the `constructor` property here, if it does not exist.\n",
    "\n",
    " 3. **Chaining the prototypes**\n",
    "\n",
    "    ``` js\n",
    "    Object.setPrototypeOf(ctor.prototype, superCtor.prototype);\n",
    "    ```\n",
    "    This statement chains the prototypes of the subclass `ctor`, and the baseclass `superCtor`. The `ctor` class now **inherits the normal properties/methods** of the `superCtor` class.\n",
    "\n",
    " 4. **Chaining the constructors**\n",
    "\n",
    "    ``` js\n",
    "    Object.setPrototypeOf(ctor, superCtor);\n",
    "    ```\n",
    "    By chaining the constructors the `ctor` class **inherits the static properties/methods** of the `superCtor` class.\n",
    "    \n",
    "So our complete code becomes:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "EasyClass = {\n",
    "    subClass(ctor, superCtor) {\n",
    "        if ( ! Object.hasOwnProperty('constructor') )\n",
    "            Object.defineProperty(ctor.prototype, 'constructor', {\n",
    "                configurable: true,\n",
    "                enumerable: false,\n",
    "                writable: true,\n",
    "                value: ctor,\n",
    "            });\n",
    "\n",
    "        Object.setPrototypeOf(ctor.prototype, superCtor.prototype);\n",
    "\n",
    "        Object.setPrototypeOf(ctor, superCtor);\n",
    "    },\n",
    "};\n",
    "\n",
    "function SuperHero(heroName, abilities) {\n",
    "    Person.call(this);\n",
    "    this.heroName = heroName;\n",
    "    this.abilities = abilities;\n",
    "}\n",
    "\n",
    "SuperHero.prototype = {\n",
    "    showAblities() {\n",
    "        console.log(\"My hero name is\", this.heroName);\n",
    "        this.abilities.forEach(ability =>\n",
    "               console.log(' - I can', ability));\n",
    "    },\n",
    "}\n",
    "\n",
    "EasyClass.subClass(SuperHero, Person);\n",
    "\n",
    "superman = new SuperHero('Superman', ['fly', 'see with X-ray']);\n",
    "superman.showAblities();\n",
    "console.log('The constructor:', superman.constructor);\n",
    "console.log('superman is a SuperHero:',\n",
    "            superman instanceof SuperHero);\n",
    "console.log('superman is a SuperHero:',\n",
    "            superman instanceof Person)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Remarks on static properties/methods\n",
    "\n",
    "Static methods are inherited, and can be overwritten in subclasses. For properties, this is also the case, but might cause unexpected behavior.\n",
    "\n",
    "A property of a baseclass can be read on the subclass."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "function BaseClass() {}\n",
    "function SubClass() {}\n",
    "EasyClass.subClass(SubClass, BaseClass);\n",
    "\n",
    "BaseClass.prop = 42;\n",
    "console.log('SubClass.prop:', SubClass.prop);"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "However, when we write to an inherited property on the subclass,\n",
    "we in essence create a new static property on the subclass."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "SubClass.prop = 99\n",
    "console.log('SubClass.prop:', SubClass.prop);\n",
    "console.log('BaseClass.prop:', BaseClass.prop);"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "This is the normal behavior for JavaScript objects."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Mixing ES5 and ES6 Classes\n",
    "\n",
    "It is easy to have an ES6 class extend an ES5 class."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "class Villain extends Person {\n",
    "    constructor() {\n",
    "        super()\n",
    "    }\n",
    "    behavior() {\n",
    "        console.log('I behave badly.')\n",
    "    }\n",
    "}\n",
    "\n",
    "ll = new Villain();\n",
    "ll.name = 'Lex Luther';\n",
    "ll.hello();\n",
    "ll.behavior();"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "However, using an ES6 class as a baseclass is a different matter."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "class Human {\n",
    "    constructor(name) {\n",
    "        this.name = name;\n",
    "    }\n",
    "    \n",
    "    hello() {\n",
    "        console.log('Hello, I am a human, and my name is', this.name);\n",
    "    }\n",
    "}\n",
    "tj = new Human('The Joker');\n",
    "tj.hello();"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "function Creep(name) {\n",
    "    Human.call(this, name);\n",
    "}\n",
    "tj = new Creep('The Joker');"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "As the 'TypeError' indicatees, the ES6 class constructors can not be called without 'new'. An attempt to save the day using\n",
    "``` js\n",
    "function Creep(name) {\n",
    "    const ret = new Human(name);\n",
    "    \n",
    "    ``` other statements initializing 'ret'\n",
    "    \n",
    "    return ret;\n",
    "}\n",
    "```\n",
    "initially seems to work, but creates problems when we try to use the `Creep` class as a baseclass in the normal way. The call `Creep.call(this, name)` will not initialize `this` as a `Creep`. The call returns an object of type `Creep`, which is not assigned to a variable, and thus garbage collected.\n",
    "\n",
    "The solution is to create new `Human`, and copying all own properties from this created `Human` into `this`. In order to do that we create the function `EasyClass.assignOwn()`."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "EasyClass.assignOwn = function (target, source) {\n",
    "\n",
    "    // copy all own non-symbol-key properties (incl non-enumerable)\n",
    "    Object.getOwnPropertyNames(source).forEach(n => {\n",
    "        Object.defineProperty(target, n, Object.getOwnPropertyDescriptor(source, n));\n",
    "    });\n",
    "\n",
    "    // copy all own symbol-key properties\n",
    "    Object.getOwnPropertySymbols(source).forEach(s => {\n",
    "        Object.defineProperty(target, s, Object.getOwnPropertyDescriptor(source, s));\n",
    "    });\n",
    "\n",
    "};\n",
    "\n",
    "function Creep(name) {\n",
    "    const human = new Human(name);\n",
    "    EasyClass.assignOwn(this, human);\n",
    "    \n",
    "    // other statements initializing 'this'\n",
    "}\n",
    "\n",
    "Creep.prototype.behavior = function () {\n",
    "    console.log('I may be human, but I am also a creep.');\n",
    "}\n",
    "\n",
    "EasyClass.subClass(Creep, Human);\n",
    "\n",
    "tj = new Creep('The Joker');\n",
    "tj.hello();\n",
    "tj.behavior();"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Mixins\n",
    "\n",
    "A **mixin** is very much like a baseclass. Typical code looks like\n",
    "``` js\n",
    "function MyMixin() {\n",
    "    // do whatever to initialize 'this'\n",
    "}\n",
    "MyMixin.prototype = {\n",
    "    // add properties/methods, but not a 'constructor' property\n",
    "};\n",
    "MyMixin.addTo = function (cls) {\n",
    "    Object.assign(cls.prototype, MyMixin.prototype);\n",
    "}\n",
    "\n",
    "function MyClass() {\n",
    "    MyMixin.call(this);\n",
    "    // add other MyClass initialization\n",
    "}\n",
    "Object.assign(MyClass.prototype, {\n",
    "    // add MyClass constants/methods\n",
    "});\n",
    "MyMixin.addTo(MyClass);\n",
    "\n",
    "```\n",
    "Remarks:\n",
    " 1. The `MyMixin()` is a function, like a baseclass function, that initializes `this` to be a suitable 'mixin' object. This function needs to be called like a baseclass initialization.\n",
    " 2. Like a baseclass, the mixin has a prototype with properties/methods. Unlike a baseclass, this prototype does not get chained, but copied into the prototype of the class it is mixed in with.\n",
    " 3. Make sure the prototype, either does not have a property `constructor`, or it does not get copied into the target class.  \n",
    " NOTE: in the example the prototype HAS a non-enumerable `constructor` property being `Object`.\n",
    " 4. Copying the prototype properties/methods is done by the static function `MyMixin.addTo(cls)`. This function used here as an example, will often be sufficient.\n",
    " NOTE: `Object.assign()` does not copy non-enumerable properties, so the `constructor` property does not get copied.\n",
    " 5. In case there are non-enumerable properties/methods, the function `EasyClass.assignOwn()` can be used. Just make sure the `constructor` property does NOT exist in this case.\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Private Variables\n",
    "\n",
    "from  [developer.mozilla.org](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Guide/Keyed_collections#weakmap_object) and [Nick Fitzgerald](https://fitzgeraldnick.com/2014/01/13/hiding-implementation-details-with-e6-weakmaps.html)\n",
    "\n",
    "``` js\n",
    "const privates = new WeakMap();\n",
    "\n",
    "function Public() {\n",
    "  const me = {\n",
    "    // Private data goes here\n",
    "  };\n",
    "  privates.set(this, me);\n",
    "}\n",
    "\n",
    "Public.prototype.method = function () {\n",
    "  const me = privates.get(this);\n",
    "  // Do stuff with private data in `me`...\n",
    "};\n",
    "\n",
    "module.exports = Public;\n",
    "\n",
    "```"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "==================================================  \n",
    "**After this block, it is meaningless.**\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "When this has been done, the baseclass can be 'connected' to our subclass. To simplify the connection between the base- and sub-class, its steps will be coded in the function `EasyClass.subClass()`. The first step this function makes is making sure our subclass has a `constructor` property."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    ", with\n",
    "constants/default values/enum properties and methods,\n",
    "and with static properties and methods.\n",
    "\n",
    "```\n",
    "// Create the function\n",
    "function MyClass() {\n",
    "    // add constructor statements here\n",
    "}\n",
    "\n",
    "// Add static properties and methods\n",
    "Object.assign(MyClass, {\n",
    "    //add them here\n",
    "});\n",
    "\n",
    "// Add the constant/default/enum properties and methods\n",
    "Object.assign(MyClass.prototype, {\n",
    "    // add them here\n",
    "});\n",
    "```\n",
    "\n",
    "There are a couple of remarks to make here:\n",
    " 1. both adding properties/methods and adding static properties/methods are optional,\n",
    " 2. Instead of using\n",
    "     ```\n",
    "     Object.assign(MyClass.prototype, {\n",
    "     ```\n",
    "     it is possible to use\n",
    "     ```\n",
    "     MyClass.prototype = {\n",
    "         constructor: MyClass,\n",
    "     ```\n",
    "\n",
    "An example and some checks."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "function First(name) {\n",
    "    console.log(`new First(${name})`);\n",
    "    this.name = name;\n",
    "}\n",
    "\n",
    "Object.assign(First.prototype, {\n",
    "    hasSuper() {\n",
    "        console.log(`has super:`, super.constructor.name);\n",
    "        console.log(`has super:`, (typeof super.toString) === 'function');\n",
    "    },\n",
    "    details() {\n",
    "        console.log(`The Firsts name is`, this.name);\n",
    "    },\n",
    "});\n",
    "\n",
    "Object.assign(First, {\n",
    "    s1() {\n",
    "        console.log(`This static function is defined in First class`);\n",
    "    },\n",
    "});"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "First.s1();\n",
    "one = new First('John');\n",
    "console.log(`Constructor:`, one.constructor.name)\n",
    "one.details();\n",
    "one.hasSuper();\n",
    "console.log(one instanceof First);"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "To subclass a function, the following `function extend()` can be used to do\n",
    "the heavy lifting. This function can also be used to extend the base class,\n",
    "using\n",
    "```\n",
    "extend(First, Object);\n",
    "```\n",
    "in which case, there is no need to define the field `constructor` in the\n",
    "prototype."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "function subClass(ctor, superCtor) {\n",
    "    \n",
    "    // Set the constructor to refer to the subclass constructor\n",
    "    if ( ! Object.hasOwnProperty('constructor') )\n",
    "        Object.defineProperty(ctor.prototype, 'constructor', {\n",
    "            configurable: true,\n",
    "            enumerable: false,\n",
    "            writable: true,\n",
    "            value: ctor,\n",
    "        });\n",
    "\n",
    "    // Chain prototypes: object properties/methods \n",
    "    Object.setPrototypeOf(ctor.prototype, superCtor.prototype);\n",
    "\n",
    "    // Chain constructors: static properties/methods\n",
    "    Object.setPrototypeOf(ctor, superCtor);\n",
    "\n",
    "}"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "function Second(name) {\n",
    "    \n",
    "    console.log(`new Second(${name})`)\n",
    "    First.call(this, name);\n",
    "}\n",
    "\n",
    "Second.prototype = {\n",
    "    constructor: Second,\n",
    "    hasSuper2() {\n",
    "        console.log(`has super2:`, super.constructor.name);\n",
    "        console.log(`has super2:`, (typeof super.toString) === 'function');\n",
    "    },\n",
    "}\n",
    "Object.assign(Second.prototype, {\n",
    "    details() {\n",
    "        console.log(`The Seconds name is`, this.name);\n",
    "    },\n",
    "    hasSuper3() {\n",
    "        console.log(`has super3:`, super.constructor.name);\n",
    "        console.log(`has super3:`, (typeof super.toString) === 'function');\n",
    "    },\n",
    "});\n",
    "\n",
    "Object.assign(Second, {\n",
    "    s2() {\n",
    "        console.log(`This static function is defined in Second class`);    \n",
    "    }\n",
    "});\n",
    "\n",
    "subClass(Second, First);"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "Second.s1();\n",
    "Second.s2();\n",
    "two = new Second(\"Ivan\");\n",
    "console.log(`Constructor:`, two.constructor.name);\n",
    "console.log(`Super:`, Object.getPrototypeOf(two.constructor).name);\n",
    "two.details();\n",
    "two.hasSuper();\n",
    "two.hasSuper2();\n",
    "two.hasSuper3();\n",
    "console.log(two instanceof Second, two instanceof First);"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "class Third extends Second {\n",
    "    constructor(name) {\n",
    "        console.log(`new Third(${name})`)\n",
    "        super(name);\n",
    "    }\n",
    "    \n",
    "    details() {\n",
    "        console.log(`The Thirds name is`, this.name);\n",
    "    }\n",
    "    \n",
    "    static s3() {\n",
    "        console.log(`This static function is defined in Third class`);    \n",
    "    }\n",
    "}"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "Third.s1();\n",
    "Third.s2();\n",
    "Third.s3();\n",
    "three = new Third(\"Kevin\");\n",
    "console.log(`Constructor:`, three.constructor.name)\n",
    "console.log(`Super:`, Object.getPrototypeOf(three.constructor).name);\n",
    "three.details();\n",
    "three.hasSuper();\n",
    "console.log(three instanceof Third, three instanceof Second, three instanceof First);"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Deriving from the Third class is a bit of a problem.\n",
    "\n",
    "The first attempt using:\n",
    "```\n",
    "function Fourth(name) {\n",
    "    Third.prototype.constructor.call(this, name);\n",
    "    ...\n",
    "```\n",
    "fails because Third's constructor can not be called without the 'new' operator.\n",
    "\n",
    "The second attempt is using:\n",
    "```\n",
    "function Fourth(name) {\n",
    "    const proto = Object.getPrototypeOf(this);\n",
    "    const obj = new Third(name);\n",
    "    Object.setPrototypeOf(obj, proto);\n",
    "    \n",
    "    // add additional constructor statements for Fourth class\n",
    "    \n",
    "    return obj;\n",
    "}\n",
    "```\n",
    "The code `new Fourth('name')` does work as expected, because the returned\n",
    "object satisfies all our needs. Unfortunately, itfails when we derive\n",
    "a Fifth class from the Fourth class. \n",
    "```\n",
    "Function Fifth(name) {\n",
    "    Fourth.call(this, name);\n",
    "```\n",
    "The last statement here, returns an object, which is not used,\n",
    "but does not initialize the Fifth's 'this' object.\n",
    "\n",
    "The final solution also has a limitation. It uses the statement\n",
    "```\n",
    "Object.assign(this, new Third(name));\n",
    "```\n",
    "\n",
    "`Object.assign()` only copies 'enumerable own properties'. The problem is\n",
    "that the non-enumerable own properties will not get copied. But this\n",
    "seems to be the best we can do."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "function Fourth(name) {\n",
    "    console.log(`new Fourth(${name})`);\n",
    "    Object.assign(this, new Third(name));\n",
    "\n",
    "}\n",
    "\n",
    "Object.assign(Fourth.prototype, {\n",
    "    details() {\n",
    "        console.log(`The Fourths name is`, this.name);\n",
    "    }\n",
    "});\n",
    "\n",
    "Object.assign(Fourth, {\n",
    "    s4() {\n",
    "        console.log(`This static function is defined in Fourth class`);    \n",
    "    },\n",
    "});\n",
    "\n",
    "subClass(Fourth, Third);"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "Fourth.s1();\n",
    "Fourth.s2();\n",
    "Fourth.s3();\n",
    "Fourth.s4();\n",
    "four = new Fourth('Leo');\n",
    "console.log(`Constructor:`, four.constructor.name)\n",
    "console.log(`Super:`, Object.getPrototypeOf(four.constructor).name);\n",
    "four.details();\n",
    "four.hasSuper();\n",
    "console.log(four instanceof Fourth, four instanceof Third, four instanceof Second, four instanceof First);\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "function Fifth(name) {\n",
    "    console.log(`new Fifth(${name})`)\n",
    "    Fourth.call(this, name);\n",
    "    \n",
    "}\n",
    "\n",
    "Object.assign(Fifth.prototype, {\n",
    "    details() {\n",
    "        console.log(`The Fifths name is`, this.name);\n",
    "    },\n",
    "});\n",
    "\n",
    "Object.assign(Fifth, {\n",
    "    s5() {\n",
    "        console.log(`This static function is defined in Fifth class`);    \n",
    "    },\n",
    "});\n",
    "\n",
    "subClass(Fifth, Fourth);"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "Fifth.s1();\n",
    "Fifth.s2();\n",
    "Fifth.s3();\n",
    "Fifth.s4();\n",
    "Fifth.s5();\n",
    "five = new Fifth('Mike');\n",
    "console.log(`Constructor:`, five.constructor.name)\n",
    "console.log(`Super:`, Object.getPrototypeOf(five.constructor).name);\n",
    "console.log(`The five's name is`, five.name);\n",
    "five.details();\n",
    "five.hasSuper();\n",
    "console.log(five instanceof Fifth, five instanceof Fourth, five instanceof Third,\n",
    "            five instanceof Second, five instanceof First);\n"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Javascript (Node.js)",
   "language": "javascript",
   "name": "javascript"
  },
  "language_info": {
   "file_extension": ".js",
   "mimetype": "application/javascript",
   "name": "javascript",
   "version": "14.17.6"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
