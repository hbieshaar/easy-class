# easy-class

The module `easy-class` simplifies using ES5 classes.

It does this by providing 2 functions. The function `subClass(subCtor, baseCtor)` will, in most cases, be the only one needed. In case you need to derive an ES5 class from a class made by the new ES6 `class` syntax, the function `assignOwn(target, source)` may be needed.

## `function subClass(subCtor, baseCtor)`
This function can be used to chain ES5 classes.

### subClass() example
This example shows a base ES5 class `Animal`, and a derived ES5 class `Bird`.
``` js
import EasyClass from 'easy-class'

function Animal() {
  // Animal initialisation
}

Animal.prototype = {
  // Animal methods, initial values and constants
}

EasyClass.subClass(Animal, Object);

function Bird() {
  Animal.call(this);

  // Other Bird initialisations
}

Bird.prototype = {
  // Bird methods, initial values and constants
}

// chain the ES5 classes
EasyClass.subClass(Bird, Animal);
```

The function `subClass()` does 3 things:

 1. it tests the existence of an own property `constructor`, and
    if it does not exist, creates an non-enumerabe property `constructor`.  
    In the example, by assigning an object to `Animal.prototype` and `Bird.prototype`, the original `constructor` properties were lost. The calls to `subClass()` recreated them.
 2. it chains set the prototype of `Bird.prototype` to `Animal.prototype`.
 3. it chains the constructor functions, i.e. it sets the prototype of `Bird()` to `Animal()`. Note: this has nothing to do with `Bird.prototype` or `Animal.prototype`.  
    This is a chaining of static properties/methods which are assigned to the `Bird()` and `Animal()` functions.

## `function assignOwn(target, source)`

This function can be used to derive a ES5 subclass from an ES6 class. The following code will not work.
``` js
class EsSix {
  constructor() {}
}

function EsFive() {
  EsSix.call(this);   // DOES NOT WORK
}
```
The reason: the constructor of an ES6 class can not be called without the `new` keyword. A "solution" that will work most of the time:
``` js
function EsFive() {
  Object.assign(this, newEsSix());
}
```
`Object.assign()` only copies own enumerable properties/methods, but that will do in most cases. The function `assignOwn()` copies all own properties/methods, and also copies all own symbols.  
Apart from in tests, I have never had to use it.

