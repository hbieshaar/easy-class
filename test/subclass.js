const EasyClass = require('../easy-class.js');

// Define the First constructor
function First(name) {
    console.log(`new First(${name})`);
    this.name = name;
}

// Add the static properties/methods
EasyClass.assignOwn(First, {
    s1() {
        console.log(`This is a static function on the First class`);
    },
});

First.prototype = {
    constructor: First,
    details() {
        console.log(`The Firsts name is`, this.name);
    }
}

// In[3]
First.s1();
one = new First('Ivan');
one.details();
console.log(one instanceof First);
console.log(one.constructor.name);

// In[4]
function Second(name) {
    console.log(`new Second(${name})`)
    First.call(this, name);
    
}

Second.s2 = function() {
    console.log(`This is a static function on the Second class`);    
}

Second.prototype = {
    details() {
        console.log(`The Seconds name is`, this.name);
    }
}

EasyClass.subClass(Second, First);

// In[5]
Second.s1();
Second.s2();
two = new Second("John");
two.details();
console.log(two instanceof Second, two instanceof First);

// In[6]
class Third extends Second {
    constructor(name) {
        console.log(`new Third(${name})`)
        super(name);
    }
    
    details() {
        console.log(`The Thirds name is`, this.name);
    }
    
    static s3() {
        console.log(`This is a static function on the Third class`);    
    }
}

// In[7]
Third.s1();
Third.s2();
Third.s3();
three = new Third("Kevin");
three.details();
console.log(three instanceof Third, three instanceof Second, three instanceof First);

// In[8]
function Fourth(name) {
    console.log(`new Fourth(${name})`)
    EasyClass.assignOwn(this, new Third(name));
    // add additional constructor statements for Fourth class
    
    // return obj;
}

Fourth.s4 = function() {
    console.log(`This is a static function on the Fourth class`);    
}

Fourth.prototype = {
    details() {
        console.log(`The Fourths name is`, this.name);
    }
}

EasyClass.subClass(Fourth, Third);

// In[9]
Fourth.s1();
Fourth.s2();
Fourth.s3();
Fourth.s4();
four = new Fourth('Leo');
four.details();
console.log(four instanceof Fourth, four instanceof Third, four instanceof Second, four instanceof First);

// In[10]
function Fifth(name) {
    console.log(`new Fifth(${name})`)
    Fourth.call(this, name);
    
}

Fifth.s5 = function() {
    console.log(`This is a static function on the Fifth class`);    
}

Fifth.prototype = {
    details() {
        console.log(`The Fifths name is`, this.name);
    }
}

EasyClass.subClass(Fifth, Fourth);

// In[11]
Fifth.s1();
Fifth.s2();
Fifth.s3();
Fifth.s4();
Fifth.s5();
five = new Fifth('Mike');
console.log(`The five's name is`, five.name);
five.details();
console.log(five instanceof Fifth, five instanceof Fourth, five instanceof Third,
            five instanceof Second, five instanceof First);